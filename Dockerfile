FROM node:alpine

WORKDIR /usr/app

COPY package*.json ./

RUN echo "bcrypt try solving"
RUN apk update
RUN apk --no-cache add --virtual builds-deps build-base python
RUN npm config set python ./bin/python
RUN npm i -g npm
RUN npm install
RUN npm rebuild bcrypt --build-from-source
RUN apk del builds-deps

COPY . .

EXPOSE 5000

RUN npm run build

RUN npm rebuild bcrypt --build-from-source

CMD ["npm", "run", "serve"]