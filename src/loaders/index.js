'use-strict';

import expressLoader from './express';
import sequelizeLoader from './sequelize';

import logger from '../utils/logger';

module.exports = async (app) => {
  try {
    sequelizeLoader.authenticate();
    expressLoader(app);
    logger.info('Database Connected');
    logger.info('Express Initialized');
  } catch (error) {
    logger.error(error.message);
    throw error;
  }
};
