import Sequelize from 'sequelize';
import logger from '../utils/logger';
import User from '../models/users';

const connection = new Sequelize(
  process.env.DATABASE,
  process.env.USERNAME,
  process.env.PASSWORD, {
    dialect: process.env.HOST,
    host: process.env.HOST,
    logging: (message) => logger.info(`Database Test ${message}`),
  },
);

User.init(connection);

module.exports = connection;
