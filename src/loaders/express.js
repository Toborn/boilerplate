'use-strict';

import bodyParser from 'body-parser';
import cors from 'cors';
import routes from '../api/index';
import logger from '../utils/logger';

module.exports = (app) => {
  app.get('/status', async (req, res) => {
    res.status(200).send({ message: 'Online' });
  });

  app.disable('x-powered-by');
  app.set('trust proxy', true);
  app.use(bodyParser.urlencoded({ extended: false, limit: '1kb' }));
  app.use(bodyParser.json({ limit: '1kb' }));
  app.use(cors());
  app.use(routes);

  app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
  });

  // eslint-disable-next-line no-unused-vars
  app.use((error, req, res, next) => {
    logger.error(error.message, error.stack);
    if (error.status) {
      res.status(error.status).send({ message: error.message });
    } else {
      res.status(400).send({ message: error.message });
    }
  });

  return app;
};
