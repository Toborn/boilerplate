module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Users', 
    [
      {
        login: 'John Doe',
        email: 'teste@teste.com',
        password: 'teste',
      },
      {
        login: 'Teste',
        email: 'teste2@teste.com',
        password: 'teste',
      }
    ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};