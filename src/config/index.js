'use-strict';

import dotenv from 'dotenv';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const envFound = dotenv.config();
if (!envFound) {
  throw new Error('⚠️  Couldn\'t find .env file  ⚠️');
}

module.exports = {
  port: process.env.PORT,
  database: process.env.DATABASE,
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
  dialect: process.env.DIALECT,
  host: process.env.HOST,
  secretkey: process.env.JWT,
  timer: process.env.TOKENTIME,
};
