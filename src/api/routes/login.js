'use-strict';

import { Router } from 'express';
import controller from '../controllers/login';

const route = Router();
module.exports = (app) => {
  app.use('/login', route);
  route.post('/', controller.login);
};
