import { Router } from 'express';
import controller from '../controllers/users';

const route = Router();
module.exports = (app) => {
  app.use('/users', route);
  route.post('/', controller.post);
  route.get('/', controller.get);
  route.get('/login/', controller.getByLogin);
  route.get('/:id', controller.getById);
  route.put('/:id', controller.put);
  route.delete('/:id', controller.delete);
};
