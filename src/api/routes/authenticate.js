'use-strict';

import controller from '../controllers/authenticate';

module.exports = (app) => {
  app.use(controller.authentication);
};
