import { Router } from 'express';
import routeLogin from './routes/login';
import routeAuthenticate from './routes/authenticate';
import routeUser from './routes/users';
import routeCostumers from './routes/costumers';
import routeHpp from './routes/hpp';
import routeHelmet from './routes/helmet';

const app = new Router();

routeHelmet(app);
routeLogin(app);
routeAuthenticate(app);
routeUser(app);
routeCostumers(app);
routeHpp(app);

module.exports = app;
