/* eslint-disable no-unused-expressions */
import service from '../../dao/users';

exports.post = (req, res, next) => {
  service.create(req.body)
    .then((result) => {
      res.status(201).send(result);
    }).catch((error) => {
      next(error);
    });
};
exports.put = (req, res, next) => {
  service.putById(req.params.id, req.body)
    .then((result) => {
      result
        ? res.status(201).send({ message: 'Usuário alterado!' })
        : res.status(402).send({ message: 'Usuário não alterado!' });
    }).catch((error) => {
      next(error);
    });
};

exports.getById = (req, res, next) => {
  service.getById(req.params.id)
    .then((result) => {
      result
        ? res.status(200).send(result)
        : res.status(404).send({ message: 'Usuário não encontrado!' });
    }).catch((error) => {
      next(error);
    });
};

exports.getByLogin = (req, res, next) => {
  service.getByLogin(req.query.login)
    .then((result) => {
      (result)
        ? res.status(200).send(result)
        : res.status(404).send({ message: 'Login não encontrado!' });
    }).catch((error) => {
      next(error);
    });
};

exports.get = (req, res, next) => {
  service.get()
    .then((result) => {
      (result.length > 0)
        ? res.status(200).send(result)
        : res.status(404).send({ message: 'Nenhum usuário encontrado!' });
    }).catch((error) => {
      console.log(error);
      next(error);
    });
};

exports.delete = (req, res, next) => {
  service.deleteById(req.params.id)
    .then((result) => {
      res.status(201).send(result);
    }).catch((error) => {
      next(error);
    });
};
