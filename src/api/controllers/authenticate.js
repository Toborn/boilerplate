'use-strict';

import service from '../../utils/jwt';

exports.authentication = async (req, res, next) => {
  try {
    const token = req.header('Authorization');
    const isValid = await service.authenticate(token);
    req.user = isValid.id;
    next();
  } catch (error) {
    next({
      error: {
        status: 403,
        message: 'Unauthorized.',
        detail: error.message,
      },
    });
  }
};
