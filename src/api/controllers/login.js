const service = require('../../utils/jwt');

exports.login = (req, res, next) => {
  service.generateToken(req.body)
    .then((result) => {
      res.status(201).send(result);
    })
    .catch((error) => {
      next({
        error: {
          status: 401,
          message: 'Login Error.',
          detail: error.message,
        },
      });
    });
};
