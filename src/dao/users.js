import model from '../models/users';

exports.create = (obj) => model.create(obj);

exports.get = () => model.findAll({
  attributes: { exclude: ['password'] },
});

exports.getById = (id) => model.findByPk(
  id,
  {
    attributes: { exclude: ['password'] },
  },
);

exports.putById = (id, objInfo) => model.update(
  objInfo,
  { where: { id } },
);

exports.deleteById = (id) => model.destroy({ where: { id } });
