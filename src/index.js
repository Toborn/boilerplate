'use-strict';

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import express from 'express';
import { port } from './config';
import loaders from './loaders';
import logger from './utils/logger';

async function startServer() {
  try {
    const app = express();
    await loaders(app);
    app.listen(port, (error) => {
      if (error) {
        logger.log({
          level: 'error',
          message: `${error}`,
        });
        process.exit();
      }
      logger.log({
        level: 'info',
        message: `Server listening on port: ${port}`,
      });
    });

    process.on('uncaughtException', (error) => {
      logger.log({
        level: 'error',
        message: `${error}`,
      });
      process.exit();
    });
  } catch (error) {
    logger.log({
      level: 'error',
      message: `${error}`,
    });
  }
}

startServer();
