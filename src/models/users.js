/* eslint-disable no-param-reassign */
import { Model, DataTypes } from 'sequelize';
import bcrypt from 'bcrypt';

class User extends Model {
  static init(connection) {
    super.init({
      login: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      instagram: DataTypes.STRING,
      facebook: DataTypes.STRING,
      type: DataTypes.STRING,
      disable: DataTypes.BOOLEAN,
    },
    {
      hooks: {
        beforeCreate: async (user) => {
          const salt = await bcrypt.genSalt(10);
          user.password = await bcrypt.hash(user.password, salt);
        },
        beforeBulkUpdate: async (user) => {
          const salt = await bcrypt.genSalt(10);
          user.attributes.password = await bcrypt.hash(user.attributes.password, salt);
        },
      },
      sequelize: connection,
    });
  }
}

module.exports = User;
