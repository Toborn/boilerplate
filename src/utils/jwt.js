'use-strict';

import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import userDao from '../dao/users';
import { secretkey, timer } from '../config/index';

exports.authenticate = async (token) => jwt.verify(token, secretkey);

exports.generateToken = ({ access, password }) => userDao.getByLogin(access)
  .then(async (user) => {
    const { login, id } = user;
    const comparation = await this.comparePassword(password, user.password);
    if (!user || !comparation) {
      throw new Error('Authentication failed!');
    }
    const payload = {
      login,
      id,
      time: new Date(),
    };
    const token = jwt.sign(payload, secretkey, { expiresIn: timer });
    return { login, token, id };
  });

exports.comparePassword = async (pw1, pw2) => {
  const result = await bcrypt.compare(pw1, pw2);
  return result;
};
