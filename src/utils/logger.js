import { createLogger, format, transports } from 'winston';

const infoFilter = format((info) => (info.level === 'info' ? info : false));

const warnFilter = format((info) => (info.level === 'warn' ? info : false));

module.exports = createLogger({
  format: format.combine(
    format.timestamp({
      format: 'DD-MM-YYYY HH:mm:ss',
    }),
    format.json(),
    format.prettyPrint(),
    format.colorize(),
  ),
  transports: [
    new transports.Console({
      format: format.combine(
        format.simple(),
        format.printf(({ level, message }) => `${level}: ${message}`),
      ),
    }),
    new transports.File({
      private: true,
      filename: `${__dirname}/../../logs/error.log`,
      level: 'error',
      maxFiles: 10,
      maxsize: 1048576,
    }),
    new transports.File({
      private: true,
      filename: `${__dirname}/../../logs/warn.log`,
      level: 'warn',
      maxFiles: 10,
      maxsize: 1048576,
      format: warnFilter(),
    }),
    new transports.File({
      private: true,
      filename: `${__dirname}/../../logs/info.log`,
      level: 'info',
      maxFiles: 10,
      maxsize: 1048576,
      format: infoFilter(),
    }),
  ],
});
