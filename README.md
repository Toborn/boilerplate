# Docker Container Name

Descrição do Container.

## Getting Started

Instruçõões do Container.

### Prerequisities


Para executar este container deverá ter o Docker instalado.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

Para utilização do ESLint deverá seguir as instruções.

* [ESLint](https://eslint.org/docs/user-guide/getting-started/)

O projeto faz uso de Banco de Dados PostgreSQL, caso o banco estiver local deverá estar instalado.

* [PostgreSQL](https://www.postgresql.org/download/)

### Usage

#### Container Parameters

List the different parameters available to your container

O projeto já possui um docker-compose configurado, conseguirá executar o projeto somente executando o código:


```shell
docker-compose up
```

Para construir a Imagem do Docker:

```shell
docker build -t local/projectname .
```

Para executar o Docker na porta 5000:

```shell
* docker run -p 5000:5000 -it -d local/projectname
```

#### Environment Variables

* `NODE_ENV` - Ambiente de desenvolvimento.
* `DATABASE` - Nome do Banco de Dados.
* `USERNAME` - Usuario de Acesso do Banco de Dados.
* `PASSWORD` - Senha de Acesso do Banco de Dados.
* `HOST` - Host do Banco de Dados.
* `DIALECT` - Banco de Dados utilizado.
* `PORT` - Porta da aplicação.
* `JWT` - Chave para criptografia.
* `TOKENTIME` - Tempo de Expiração do token de Acesso.

#### Volumes

* `/usr/app` - Local da Aplicação

#### Useful File Locations

* `/usr/app/logs` - Logs da Aplicação

## Built With

* node v11.13.0
* docker v20.10.1
* docker-compose v1.25.5
* postgres v13.1

## Find Us

* [GitLab](https://gitlab.com/Toborn/boilerplate)

## Versioning

Utilizamos [SemVer](http://semver.org/) para versionamento.

## Authors

* **Marcio Braga** - *Initial work* - [Toborneiro](https://gitlab.com/Toborn)


## Acknowledgments

* People you want to thank
* If you took a bunch of code from somewhere list it here
